# geo-tools-ui

Geologist Dashboard control

# Angular Cli commands

## Uninstall and install angular cli

```
npm uninstall -g @angular/cli
npm cache clean --force
npm install -g @angular/cli
ng --version
```

## Update local angular cli

```
ng --version
npm uninstall --save-dev angular-cli
npm install --save-dev @angular/cli@latest
ng --version
npm i
npm start
ng update @angular/core
ng update rxjs

```

## Update local angular cli v2
https://www.positronx.io/update-angular-cli-latest-version-7-8/

```
ng --version
npm uninstall -g angular-cli
npm cache clean
npm install -g @angular/cli@latest
ng update @angular/cli
npm cache verify
npm cache clean
```

## fix local issues

```
npm audit
npm audit fix
```

## Deploy Angular 8/9 App to Firebase

https://www.positronx.io/setup-angularfire2-library-in-angular-7-project/
https://www.positronx.io/deploy-angular-8-app-to-firebase/
https://console.firebase.google.com/

```
npm install firebase @angular/fire --save
npm install -g firebase-tools
firebase login
firebase init
ng build --prod --aot
firebase deploy
```

geology-eade

# Adding Features to Your Angular Application

You can use the ng generate command to add features to your existing application:

- `ng generate class my-new-class`: add a class to your application
- `ng generate component my-new-component`: add a component to your application
- `ng generate directive my-new-directive`: add a directive to your application
- `ng generate enum my-new-enum`: add an enum to your application
- `ng generate module my-new-module`: add a module to your application
- `ng generate pipe my-new-pipe`: add a pipe to your application
- `ng generate service my-new-service`: add a service to your application

The generate command and the different sub-commands also have shortcut notations, so the following commands are similar:

- `ng g cl my-new-class`: add a class to your application
- `ng g c my-new-component`: add a component to your application
- `ng g d my-new-directive`: add a directive to your application
- `ng g e my-new-enum`: add an enum to your application
- `ng g m my-new-module`: add a module to your application
- `ng g p my-new-pipe`: add a pipe to your application
- `ng g s my-new-service`: add a service to your application.

# References

- [png-to-svg](https://onlineconvertfree.com/convert-format/png-to-svg)
- [Readme.md syntax](https://help.github.com/es/github/writing-on-github/basic-writing-and-formatting-syntax)
- [simple-line-icons](https://coreui.io/v1/demo/AngularJS_Demo/#!/icons/simple-line-icons)
- [The Ultimate Angular CLI Reference Guide](https://www.sitepoint.com/ultimate-angular-cli-reference)
- [Angular 8 - JWT Authentication Example & Tutorial](https://jasonwatmore.com/post/2019/06/22/angular-8-jwt-authentication-example-tutorial)

# Steps

```
cd src/app/routes
ng g m admin
cd admin
```